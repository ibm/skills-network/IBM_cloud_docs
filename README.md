This repo contains HTML fils that refernce  IBM cloud docs, note there is a legacy repo so you must update legacy repo as well by following :

We have two Git repositories and you would like to keep things sync between them

- Current: `git@gitlab.com:ibm/skills-network/IBM_cloud_docs.git`
- Legacy: `git@gitlab.com:ibm-skills-network/IBM_cloud_docs.git`

You already have `git@gitlab.com:ibm/skills-network/IBM_cloud_docs.git` as `origin` in your local Git repository.

## Add the legacy Git repository as another remote

```sh
$ git remote add legacy git@gitlab.com:ibm-skills-network/IBM_cloud_docs.git
```

## Push changes

```sh
# Push to the current repo
$ git push origin master

# Also push to the legacy repo
$ git push legacy master
```